section .text
 

exit: 
    mov rax, 60 
    syscall 


string_length:
    xor rax,rax 
    .loop: 
        cmp byte[rax+rdi],0 
        je .stop
        inc rax 
        jmp .loop
    .stop:
        ret


print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1 
    mov rdi, 1 
    syscall
    ret


print_char: 
    push rdi 
    mov rdx, 1     
    mov rsi, rsp 
    pop rdi
    mov rdi, 1  
    mov rax, 1    
    syscall     
    ret


print_newline:
    xor rax, rax
    mov rdi, '\n'
    jmp print_char


print_uint:
    xor rax, rax 
    mov r9, rsp
    mov r8, 10
    mov rax, rdi 
    push 0 
    .loop:
        xor rdx,rdx 
        div r8 
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl 
        cmp rax,0
        je .stop
        jmp .loop 

    .stop:
        mov rdi, rsp 
        push r9
        call print_string
        pop r9
        mov rsp, r9
        ret

print_int:
    xor rax, rax
    cmp rdi,0
    jl .neg 
    jmp print_uint 

    .neg
        push rdi  
        mov rdi, '-' 
        call print_char
        pop rdi
        neg rdi 
        jmp print_uint


string_equals:
    xor rax, rax
    xor rcx, rcx 
    xor r9, r9
    xor r8, r8
    .loop: 
        mov r9b, byte[rsi+rcx] 
        mov r8b, byte[rdi+rcx] 
        cmp r8b,r9b 
        jne .no 
        cmp r8b,0
        je .yes
        inc rcx 
        jmp .loop
    
    .yes:
        mov rax, 1  
        ret
    .no:
        mov rax,0 
        ret

read_char:
    xor rdi,rdi 
    xor rax, rax 
    mov rdx, 1
    push 0 
    mov rsi, rsp 
    syscall
    pop rax 
    ret 

read_word:
    xor rax, rax
    xor rcx, rcx
    .loop:
        push rcx
        push rsi
        push rdi
        call read_char 
        pop rdi
        pop rsi
        pop rcx
        cmp rax, 0 
        je .stop
        cmp rax, ' ' 
        je .escape_freedom
        cmp rax, `\t`
        je .escape_freedom
        cmp rax, '\n'
        je .escape_freedom

        mov [rdi+rcx], rax
        inc rcx
        cmp rcx, rsi
        jge .err
        jmp .loop

    .escape_freedom:
        cmp rcx,0 
        je .loop
        jmp .stop 
    .err:             
        xor rdx, rdx                                      
        xor rax, rax
        ret
    .stop:
        xor rax, rax
        mov [rdi+rcx], rax 
        mov rdx, rcx 
        mov rax, rdi 
        ret
    


parse_uint:
    xor rcx, rcx
    xor rax, rax
    mov r8, 10 
    .loop: 
        movzx r9, byte[rdi+rcx] 
        cmp r9,0 
        je .stop
        cmp r9b, '0' 
        jl .stop     
        cmp r9b, '9'
        jg .stop

        mul r8 
        sub r9b, '0' 
        add rax, r9 
        inc rcx 
        jmp .loop

    .stop
        mov rdx, rcx
        ret

parse_int:
    xor rax, rax
    xor rdx, rdx 
    mov rcx, rdi 
    cmp byte[rcx], '-'
    je .neg
    jmp .pos 
    .neg:
        inc rcx
        mov rdi, rcx
        push rcx
        call parse_uint 
        pop rcx
        neg rax 
        inc rdx
        ret
    .pos:
        mov rdi, rcx
        jmp parse_uint
        

string_copy: 
    xor rax, rax
    push rsi
    push rdi
    push rcx
    push rdx
    call string_length
    xor rcx, rcx
    pop rdx
    pop rcx
    pop rdi
    pop rsi

    mov r8, rax 
    cmp rdx, r8 
    jl .err
    .loop:
        cmp rcx, r8
        jg .stop
        mov r10,[rdi+rcx] 
        mov [rsi+rcx], r10
        inc rcx
        jmp .loop
    
    .err:
        xor rax, rax 
        ret
    .stop:
        mov rax, r8 
        ret
